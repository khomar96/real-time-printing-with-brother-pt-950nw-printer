
from reportlab.lib.styles import getSampleStyleSheet,ParagraphStyle
from reportlab.lib import styles
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4,letter
from reportlab.lib.units import cm,inch
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle,Image,Spacer
from reportlab.platypus.paragraph import Paragraph
from reportlab.lib.enums import TA_RIGHT, TA_CENTER ,TA_LEFT
from Label.LabelLibrary.QrCode import * 
import datetime
from PIL import Image as im


directory='./'


def UniqueFileName():#generates a unique pdf filename
     uniq_filename = str(datetime.datetime.now().date()) + '_' + str(datetime.datetime.now().time()).replace(':', '.')
     return uniq_filename
    

def LabelCreating(SN,IMEI,ProductName):
    FileName=UniqueFileName()+'.pdf'
    doc = SimpleDocTemplate(FileName, pagesize=A4)
    paragraph="<br />\n"+"<br />\n"+" S/N: "+SN+"<br />\n"+" IMEI: "+IMEI+"<br />\n"#for the label(<br />\n to break the line)
    paragraph2="<b>"+ProductName+"</b>"#for the productname
    paragraphQR="\n "+SN+"\n"+"\n" #for the QR
    paragraphLogos="<br />\n"
    elements = []
    Qr=QrCode(paragraphQR,UniqueFileName()+'.png')
    ImQr=Image(Qr.GetQrName())#Open the QR image code
    ImTrash=Image(directory+'Label/Icones/Trash.jpg')#Open the trash image code
    ImCe=Image(directory+'Label/Icones/ce.png')#Open the trash image code
    ImQr.drawHeight = 1.2*inch*ImQr.drawHeight / ImQr.drawWidth #set the  height of the QR
    ImQr.drawWidth = 1.2*inch#set the  width of the QR
    ImTrash.drawHeight = 0.49*inch*ImTrash.drawHeight / ImTrash.drawWidth #set the  height of the QR
    ImTrash.drawWidth = 0.44*inch#set the  width of the QR
    ImCe.drawHeight = 0.44*inch*ImCe.drawHeight / ImCe.drawWidth #set the  height of the QR
    ImCe.drawWidth = 0.45*inch#set the  width of the QR
    styles=getSampleStyleSheet()
    sp = ParagraphStyle('PS')#customize the style
    sp.fontSize=11#the fontsize set to 12
    sp.alignment = TA_LEFT#shift text into the left
    sp.leftIndent = 5
    para=Paragraph(paragraph,sp)
    paragraphLogos=Paragraph("<br />\n",sp)
    sp2 = ParagraphStyle('PS2')#customize the style
    sp2.fontSize=16#the fontsize set to 12
    sp2.leftIndent =5
    sp2.alignment = TA_LEFT#shift text into the left 
    para2=Paragraph(paragraph2,sp2)
    data0= [[[ImQr],[ImCe,paragraphLogos,ImTrash],[para2,para]]]#The number of columns is  determined by the number of items inside data0
    t0=Table(data0,colWidths=[3*cm,1.75*cm,6.1*cm],rowHeights=[3*cm])#put the data inside the table+sets the col width and the rows 
    t0.setStyle(TableStyle([('LINEAFTER',(-1,-1),(-1,-1),3,colors.white),('LINEBEFORE',(0,0),(0,0),3,colors.white),('LINEABOVE',(0,0),(-1,-1),3,colors.white),('INNERGRID',(1,0),(2,0),0.5,colors.grey),('VALIGN',(0,0),(-1,-1),'CENTER'),('LINEBELOW',(0,0),(-1,-1),3,colors.white)]))#set the table type and its color+thickness of the lines(0.1)
    elements.append(t0)
    doc.build(elements)#include the table inside the pdf file
    print("Label created\n")
    return FileName


