
import pyqrcode
import png





class QrCode:

    def __init__(self,Paragraph,QrName):
        qr=pyqrcode.create(Paragraph,'Q')
        self.QrName="QR"+QrName
        qr.png(self.QrName,scale=6)
        print('QR Code generated')
        
    
    def GetQrName(self):
        return self.QrName
