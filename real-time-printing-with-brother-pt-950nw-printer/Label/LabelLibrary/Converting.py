



import pdf2image 
import time
from random import random

DPI = 200
OUTPUT_FOLDER = None
FIRST_PAGE = None
LAST_PAGE = None
FORMAT = 'jpg'
THREAD_COUNT = 1
USERPWD = None
USE_CROPBOX = False
STRICT = False

directory='./'
Saving_Path=directory+'Label/Lbls/'


def pdftopil(PathToPdf,OutPut_Folder):
    pil_images=pdf2image.convert_from_path(PathToPdf,dpi=DPI,output_folder=OutPut_Folder,first_page=1)
    return pil_images
    


def save_images(pil_images):
    index=1

    for image in pil_images:
        imageFileName=Saving_Path+"label_"+str(index)+".jpg"
        image.save(imageFileName)
        index+=1
        return imageFileName



def Converting_Launcher(PathToPdf,OutPut_Folder):
    pil_images=pdftopil(PathToPdf,None)
    ImageFileName=save_images(pil_images)
    return ImageFileName
