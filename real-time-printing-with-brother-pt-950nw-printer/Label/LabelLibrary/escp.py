

import socket
import struct
from PIL import Image
from random import random



def escp(density_code=72):
    stack_size_in_bytes={72: 6}[density_code]
    height = 200
    width = 130

    yield b'\x1bia\x00'  # To enter ESC/P command mode: ESC/P standard
    yield b'\x1b@'  # To initialize the printer
    yield b'\x1bim\x00\x00'  # Fix the margin to  0
    yield b'\x1biXE2\x00\x00\x00'  # barcode margin: 0(Useless in our case)
    yield b'\x1b3' + struct.pack('!B', 24)  # line feed length: 24 dots (i.e. no space between lines)

    for y_offset in range(0, height, 8 * stack_size_in_bytes):
        yield b'\x1b*' + struct.pack('!B', density_code) + struct.pack('<H', width)
        yield b'\xff' * width * stack_size_in_bytes
        yield b'\x0a'  # linefeed (move position 24 dots down)
    yield b'\x0c' # Print start


    
    
def CropLabel(PathToImage,Coordinates):
    print("from the escp file")
    img=Image.open(PathToImage)
    Cropped_Image=img.crop(Coordinates)
    CroppedName='label_'+str(random())+'.jpg'
    Cropped_Image.save(CroppedName)
    print("From the escp file",CroppedName)
    return CroppedName




    
    

